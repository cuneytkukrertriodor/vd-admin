﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VideoDistribution.Json
{
    public class TunnlResponse
    {
        public int ResponseCode { get; set; }

        public string ResponseText { get; set; }

        public Data Data { get; set; }

        public bool IsValid { get; set; }
    }

    public class Data
    {
        public bool Result { get; set; }
    }
}