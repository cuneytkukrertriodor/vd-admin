import { mandatory } from '../modules/mandatory';
import { extendDefaults } from '../modules/extendDefaults';

class Carousel {

    constructor(element = mandatory(), options) {

        // Set defaults, which could be overwritten.
        var defaults = {
            speed: 500,
            delay: 4000
        };

        // Create options by extending defaults with the passed in arguments.
        if (options) {
            this.options = extendDefaults(defaults, options);
        } else {
            this.options = defaults;
        }

        // Get our needed elements.
        this.targetContainer = element || false;

        // Check for missing elements.
        if (!this.targetContainer) {
            throw new Error('Missing a carousel Target element!');
        }

        this.timer = null;
        this.initialPosition = 0;
        this.offset = 0;
    }

    create() {
        var slide = this.targetContainer.parentNode;
        var images = this.targetContainer.getElementsByTagName('img');
        var imageWidth = images[0].offsetWidth;
        this.initialPosition = (slide.offsetWidth + imageWidth);
        this.offset = imageWidth;

        // Set total width of all our images within the slide.
        if (images != null) {
            var totalWidth = 0;
            for (var i = 0; i < images.length; i++) {
                totalWidth += images[i].offsetWidth;
            }
            this.targetContainer.style.width = totalWidth + 'px';
        }

        // Center our slide.
        this.targetContainer.style.transform = 'translateX(-' + this.initialPosition + 'px)';

        // Now start playing the carousel.
        this.play();
    }

    next(callback) {
        this.pause();
        this._move('next', function() {
            callback();
        });
    }

    previous(callback) {
        this.pause();
        this._move('previous', function() {
            callback();
        });
    }

    pause() {
        window.clearInterval(this.timer);
    }

    play() {
        this.timer = window.setInterval(() => {
            this._move('next');
        }, this.options.delay);
    }

    _move(direction, callback) {
        var offset = (direction === 'next') ? this.initialPosition + this.offset : this.targetContainer.parentNode.offsetWidth;
        // Animate the slide.
        this.targetContainer.style.transition = 'transform ' + this.options.speed + 'ms cubic-bezier(0.55, 0, 0.1, 1)';
        this.targetContainer.style.transform = 'translateX(-' + offset + 'px)';
        setTimeout(() => {
            // Now move the first image to the back or visa versa.
            if (direction === 'next') {
                this.targetContainer.insertBefore(this.targetContainer.firstElementChild, null);
            } else {
                this.targetContainer.insertBefore(this.targetContainer.lastElementChild, this.targetContainer.firstElementChild);
            }

            // Set the slide back to the starting position.
            this.targetContainer.style.transition = 'transform 0s';
            this.targetContainer.style.transform = 'translateX(-' + this.initialPosition + 'px)';

            if (typeof callback === 'function') {
                callback();
            }
        }, this.options.speed);
    }
}

export default Carousel;





