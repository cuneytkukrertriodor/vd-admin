window._redirectListeners = [];
function addRedirectListener(callbackFunction, callbackObject, context) {
    if (window._redirectListeners === null) {
        callbackFunction.call(context || window, callbackObject);
    } else {
        window._redirectListeners.push({
            callbackFunction: callbackFunction,
            callbackObject: callbackObject,
            context: (context || window)
        });
    }
}

export { addRedirectListener }