import Page from '../pages/page';
import Carousel from '../components/carousel';

let instance = null;

class HomePage extends Page {

    static get instance() {
        return instance;
    }

    constructor() {
        super();

        // Make this a singleton.
        if (instance) {
            return instance;
        } else {
            instance = this;
        }

        return instance;
    }

    enableCarousel() {
        // Todo: Only create the carousel when $small-bp is reached as min window size. Because below that we only want the user to swipe.
        var carouselElement = document.getElementById('carousel');
        var carousel = new Carousel(carouselElement);
        carousel.create();

        // Bind our next and previous buttons.
        var nextButtons = document.getElementsByClassName('next');
        if (typeof nextButtons !== 'undefined' && nextButtons != null) {
            for (var j = 0; j < nextButtons.length; j++) {
                nextButtons[j].addEventListener('click', (e) => {
                    e.target.setAttribute('disabled', 'disabled');
                    carousel.next(function() {
                        e.target.removeAttribute('disabled');
                    });
                }, false);
            }
        }
        var previousButtons = document.getElementsByClassName('previous');
        if (typeof previousButtons !== 'undefined' && previousButtons != null) {
            for (var k = 0; k < previousButtons.length; k++) {
                previousButtons[k].addEventListener('click', (e) => {
                    e.target.setAttribute('disabled', 'disabled');
                    carousel.previous(function() {
                        e.target.removeAttribute('disabled');
                    });
                }, false);
            }
        }

        // Bind our cursor to pause the carousel.
        carouselElement.addEventListener('mouseenter', function() {
            carousel.pause();
        });
        carouselElement.addEventListener('mouseleave', function() {
            carousel.play();
        });
    }
}

export default HomePage;