﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using VideoDistribution.Models;

namespace VideoDistribution.Controllers
{
    [Authorize(Roles = "Superadmin,Admin")]
    public class ReportController : Controller
    {

        public ActionResult Index()
        {
            return View();
        }

        public JsonResult GetReport(string sortdatafield, string sortorder, int pagesize, int pagenum)
        {

            using (VideoDistributionDataContext db = new VideoDistributionDataContext())
            {
                var query = Request.QueryString;
                var dbResult = db.ExecuteQuery<view_ADM_ReportNotification>(Utils.Utils.BuildQuery(query, "view_ADM_ReportNotification")).OrderByDescending(g => g.AddedDate).ToList();
                var videos = from video in dbResult
                             select new view_ADM_ReportNotification
                             {
                                 AddedDate=video.AddedDate,
                                 Name=video.Name,
                                 Title=video.Title,
                                 Url=video.Url,
                                 Id=video.Id,
                                 IsResolved=video.IsResolved
                             };
                var total = dbResult.Count();
                videos = videos.Skip(pagesize * pagenum).Take(pagesize);
                if (sortdatafield != null && sortorder != "")
                {
                    videos =Utils.Utils.SortOrders(videos, sortdatafield, sortorder);
                }
                var result = new
                {
                    TotalRows = total,
                    Rows = videos
                };
                return Json(result, JsonRequestBehavior.AllowGet);

            }
        }

        public JsonResult ResolveReportedVideo(string title)
        {
            StringBuilder result = new StringBuilder();
            
            try
            {
                if (string.IsNullOrEmpty(title))
                {
                    result.AppendLine("Reported video must have a Title.");
                    return Json(new { result = result.ToString() });
                }
                
                using (var db = new VideoDistributionDataContext())
                {
                    db.ADM_ResolveRelatedVideo(title);
                    result.AppendLine("RESOLUTION IS SUCCESSFUL!!!");
                }
            }
            catch (Exception ex)
            {
                result.AppendLine("RESOLUTION HAS ERROR!!!");
                result.AppendLine(ex.Message);
            }
            
            return Json(new { result = result.ToString() });
        }
    }
}