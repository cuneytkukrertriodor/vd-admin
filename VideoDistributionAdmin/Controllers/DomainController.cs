﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using VideoDistribution.Models;

namespace VideoDistribution.Controllers
{
    public class DomainController : Controller
    {
        // GET: Domain
        public ActionResult Index()
        {
            return View();
        }

        public JsonResult GetReport(string sortdatafield, string sortorder, int pagesize, int pagenum)
        {

            using (VideoDistributionDataContext db = new VideoDistributionDataContext())
            {
                var query = Request.QueryString;
                var dbResult = db.ExecuteQuery<view_ADM_ReportNotification>(Utils.Utils.BuildQuery(query, "view_ADM_ReportNotification")).OrderByDescending(g => g.AddedDate).ToList();
                var videos = from video in dbResult
                             select new view_ADM_ReportNotification
                             {
                                 AddedDate = video.AddedDate,
                                 Name = video.Name,
                                 Title = video.Title,
                                 Url = video.Url,
                                 Id = video.Id
                             };
                var total = dbResult.Count();
                videos = videos.Skip(pagesize * pagenum).Take(pagesize);
                if (sortdatafield != null && sortorder != "")
                {
                    videos = Utils.Utils.SortOrders(videos, sortdatafield, sortorder);
                }
                var result = new
                {
                    TotalRows = total,
                    Rows = videos
                };
                return Json(result, JsonRequestBehavior.AllowGet);

            }
        }
    }
}