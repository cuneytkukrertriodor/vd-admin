﻿using System.Web.Mvc;

namespace VideoDistribution.Controllers
{
    [Authorize(Roles = "Superadmin,Admin")]
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return RedirectToAction("Index", "Videos");
        }
    }
}