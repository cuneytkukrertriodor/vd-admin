﻿using VideoDistribution.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace VideoDistribution.Controllers
{
    [Authorize(Roles = "SuperAdmin,Admin")]
    public class UsersController : Controller
    {
        [Authorize(Roles = "SuperAdmin")]
        public ViewResult Users(int? id)
        {
            return View();
        }

        [HttpGet]
        public ActionResult Videos(string id)
        {
            ViewBag.Id = id;
            return View("UserVideos");
        }

        public JsonResult GetVideoById(string sortdatafield, string sortorder, int pagesize, int pagenum, string id)
        {
            using (var db = new VideoDistributionDataContext())
            {
                var query = Request.QueryString;
                var dbResult = db.ExecuteQuery<view_ADM_Video>(Utils.Utils.BuildQuery(query, "view_ADM_Videos")).Where(a => a.UserId == int.Parse(id)).OrderByDescending(g => g.AddedDate).ToList();
                var videos = from video in dbResult
                            select new view_ADM_Video
                            {
                                Id = video.Id,
                                VideoMd5 = video.VideoMd5,
                                Title = video.Title,
                                Warning = video.Warning,
                                Approved = video.Approved,
                                Visible = video.Visible,
                                Banner = video.Banner,
                                ExternalThumbURL = video.ExternalThumbURL,
                                AddedDate = video.AddedDate,
                                CompanyName = video.CompanyName,
                                UserId = video.UserId
                            };
                var total = dbResult.Count();
                videos = videos.Skip(pagesize * pagenum).Take(pagesize);
                if (sortdatafield != null && sortorder != "")
                {
                    videos = Utils.Utils.SortOrders<view_ADM_Video>(videos, sortdatafield, sortorder);
                }
                var result = new
                {
                    TotalRows = total,
                    Rows = videos
                };
                return Json(result, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public ActionResult EditVideo(string id)
        {
            var db = new VideoDistributionDataContext();
            var result = db.ADM_VideoById(id).FirstOrDefault();

            ViewBag.Category = new SelectList(db.GetCategories(), "Id", "Title", result.CategoryId);
            ViewBag.Company = new SelectList(db.STAT_getUsersList("s1").OrderBy(u => u.CompanyName), "Id", "CompanyName", result.UserId);
            return View(result);
        }

        [HttpPost]
        public ActionResult EditVideo(ADM_VideoByIdResult result)
        {
            var db = new VideoDistributionDataContext();
            db.UpdateVideo(
                result.UserId,
                result.VideoMd5,
                result.Title,
                Convert.ToBoolean(null),
                Convert.ToBoolean(result.Visible),
                Convert.ToBoolean(null),
                result.Warning,
                result.VideoType,
                result.Width,
                result.Height,
                result.ExternalURL,
                result.Description,
                result.Filename,
                Convert.ToInt32(result.Category),
                Convert.ToInt32(result.Privacy)
            );
            ViewBag.Id = result.UserId;
            return View("UserVideos");
        }

        [HttpGet]
        public ActionResult UpdateUsers(int? id)
        {
            using (var db = new VideoDistributionDataContext())
            {
                var result = db.ADM_UsersById(id).FirstOrDefault();
                return View("EditUser", result);
            }

        }
        [HttpPost]
        public ActionResult UpdateUsers(ADM_UsersByIdResult result)
        {
            var db = new VideoDistributionDataContext();
            db.ADM_UpdateUsers(
                result.Id,
                result.Name,
                result.Lastname,
                result.Email,
                result.Password,
                result.WebsiteURL,
                Convert.ToBoolean(result.Active),
                result.PartnerId,
                result.DeveloperId,
                result.UserId
            );
            return RedirectToAction("Users");
        }

        public JsonResult UpdateUserVideosInline(ADM_VideoByIdResult result)
        {
            using (var db = new VideoDistributionDataContext())
            {
                db.UpdateVideo(null,result.VideoMd5,result.Title,Convert.ToBoolean(null),Convert.ToBoolean(result.Visible),Convert.ToBoolean(null),
                    result.Warning,result.VideoType,result.Width,result.Height,result.ExternalURL,result.Description,result.Filename,result.CategoryId, result.PrivacyId);
                return Json(true, JsonRequestBehavior.AllowGet);
            }
        }
    }
}