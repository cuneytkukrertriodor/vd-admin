﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using VideoDistribution.Models;
using VideoDistribution.Utils;

namespace VideoDistribution.Controllers
{
    public class PerformanceController : Controller
    {
        [Authorize(Roles = "Superadmin")]
        public ActionResult Index(DateTime? from , DateTime? to)
        {
            using (VideoDistributionDataContext db = new VideoDistributionDataContext())
            {
                if (from.HasValue && to.HasValue)
                {
                    var x = db.ADM_PerformanceReport(Utils.Utils.FormatDate((DateTime)from), Utils.Utils.FormatDate((DateTime)to)).ToList();
                    ViewBag.List = x;
                    ViewBag.from = from;
                    ViewBag.to = to;
                }
            }

            return View();
        }

        [Authorize(Roles = "Superadmin")]
        public JsonResult GetCurrentMonthAndWeekPerformance()
        {
            var now = DateTime.Now;
            var weekStart = DateTime.Now.StartOfWeek(DayOfWeek.Monday);
            string currentMonthStart = now.Year.ToString() + "-" + now.Month.ToString() + "-01";

            using (VideoDistributionDataContext db = new VideoDistributionDataContext())
            {
                var monthlyResult = db.ADM_GetTotalPerformance(currentMonthStart, Utils.Utils.FormatDate(now)).FirstOrDefault();
                var weeklyResult = db.ADM_PerformanceReport(Utils.Utils.FormatDate(weekStart), Utils.Utils.FormatDate(weekStart.AddDays(6))).ToList();
                var finalresult = new
                {
                    Monthly = monthlyResult,
                    Weekly = weeklyResult
                };
                return Json(finalresult, JsonRequestBehavior.AllowGet);
            }
        }
    }
}