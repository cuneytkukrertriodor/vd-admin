﻿using VideoDistribution.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace VideoDistribution.Controllers
{
    [Authorize(Roles = "Superadmin,Admin")]
    public class FeaturedVideosController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult RemoveFeature(int id)
        {
            var db = new VideoDistributionDataContext();
            var result = db.ADM_RemoveFeaturedVideo(id);
            return RedirectToAction("Index", "FeaturedVideos");
        }

        public JsonResult GetFeaturedVideos(string sortdatafield, string sortorder, int pagesize, int pagenum)
        {
            using (var db = new VideoDistributionDataContext())
            {
                var query = Request.QueryString;
                var dbResult = db.ExecuteQuery<view_ADM_FeaturedVideo>(Utils.Utils.BuildQuery(query, "view_ADM_FeaturedVideos")).OrderByDescending(g=>g.AddedDate).ToList();
                var featuredVideos = from featuredVideo in dbResult
                            select new view_ADM_FeaturedVideo
                            {
                                Id = featuredVideo.Id,
                                Title = featuredVideo.Title,
                                VideoMd5 = featuredVideo.VideoMd5,
                                Warning = featuredVideo.Warning,
                                Visible = featuredVideo.Visible,
                                Approved = featuredVideo.Approved,
                                AddedDate = featuredVideo.AddedDate,
                                Company = featuredVideo.Company,
                                Order = featuredVideo.Order,
                                CompanyOrder = featuredVideo.CompanyOrder
                            };
                var total = dbResult.Count();
                featuredVideos = featuredVideos.Skip(pagesize * pagenum).Take(pagesize);
                if (sortdatafield != null && sortorder != "")
                {
                    featuredVideos = Utils.Utils.SortOrders<view_ADM_FeaturedVideo>(featuredVideos, sortdatafield, sortorder);
                }
                var result = new
                {
                    TotalRows = total,
                    Rows = featuredVideos
                };
                return Json(result, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult UpdateFeaturedVideos(GetFeaturedVideos_2017_05_16Result result)
        {
            using (var db = new VideoDistributionDataContext())
            {
                db.UpdateFeaturedVideo(
                    result.Id.ToString(), 
                    result.Title, 
                    result.VideoMd5, 
                    Convert.ToBoolean(result), 
                    Convert.ToBoolean(result.Visible), 
                    Convert.ToBoolean(result.Banner),
                    result.Warning, 
                    Convert.ToBoolean(result.Analytics), 
                    result.Order, 
                    result.CompanyOrder
                );
                return Json(true, JsonRequestBehavior.AllowGet);
            }
        }
    }
}