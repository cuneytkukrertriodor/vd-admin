﻿using VideoDistribution.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using static VideoDistribution.IdentityConfig;
using System.Net;
using System.Net.Http;
using System.Web.Script.Serialization;
using VideoDistribution.Json;
using System.Text;
using static VideoDistribution.Enums.VideoDistAdminEnums;

namespace VideoDistribution.Controllers
{
    public class AccountController : Controller
    {
        private ApplicationSignInManager _signInManager;
        private ApplicationUserManager _userManager;

        public ApplicationSignInManager SignInManager
        {
            get
            {
                return _signInManager ?? HttpContext.GetOwinContext().Get<ApplicationSignInManager>();
            }
            private set
            {
                _signInManager = value;
            }
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        public string SortDataField
        {
            get
            {
                var sortDataField = "Id";
                if (Request.QueryString.GetValues("sortdatafield") != null && Request.QueryString.GetValues("sortdatafield").ToList().Count > 0)
                    sortDataField = Request.QueryString.GetValues("sortdatafield")[0];
                return sortDataField;
            }
        }

        public string SortOrder
        {
            get
            {
                var sortOrder = "DESC";
                if (Request.QueryString.GetValues("sortorder") != null && Request.QueryString.GetValues("sortorder").ToList().Count > 0)
                    sortOrder = Request.QueryString.GetValues("sortorder")[0].ToUpper();
                return sortOrder;
            }
        }

        public AccountController()
        {
        }

        public AccountController(ApplicationUserManager userManager, ApplicationSignInManager signInManager)
        {
            UserManager = userManager;
            SignInManager = signInManager;
        }

        public ActionResult Index()
        {
            return RedirectToAction("Login");
        }

        [HttpGet]
        [AllowAnonymous]
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "None")]
        public ActionResult Login(string returnUrl)
        {
            ViewBag.ReturnUrl = returnUrl;
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Login(LoginViewModel model, string returnUrl)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            var db = new VideoDistributionDataContext();
            var user = db.GetAdmins().Where(u => u.Email == model.Username && u.Password == model.Password).FirstOrDefault();

            if (user != null)
            {

                FormsAuthentication.SetAuthCookie(model.Username, false);

                var authTicket = new FormsAuthenticationTicket(1, user.Email, DateTime.Now, DateTime.Now.AddMinutes(1440), false, user.RoleName);
                string encryptedTicket = FormsAuthentication.Encrypt(authTicket);
                var authCookie = new HttpCookie(FormsAuthentication.FormsCookieName, encryptedTicket);
                HttpContext.Response.Cookies.Add(authCookie);

                var userCookie = new HttpCookie("userCookie");//instantiate an new cookie and give it a name
                userCookie.Values.Add("userid", user.Id.ToString());//populate it with key, value pairs
                Response.Cookies.Add(userCookie);//add it to the client
                return RedirectToAction("Index", "Home");
            }

            else
            {
                ViewBag.Error = "Wrong Email or Password!!";
                ModelState.AddModelError("", "Invalid credentials");
                return View(model);
            }


        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult LogOut()
        {
            //AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
            FormsAuthentication.SignOut();
            return RedirectToAction("Index", "Home");
        }

        [AllowAnonymous]
        public async Task<ActionResult> ConfirmEmail(string userId, string code)
        {
            if (userId == null || code == null)
            {
                return View("Error");
            }
            var result = await UserManager.ConfirmEmailAsync(userId, code);
            return View(result.Succeeded ? "ConfirmEmail" : "Error");
        }

        [AllowAnonymous]
        public ActionResult ForgotPassword()
        {
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ForgotPassword(ForgotPasswordViewModel model)
        {
            if (Utils.Utils.validateEmail(Utils.Utils.safeStr(model.Email)))
            {
                ViewBag.message = "";
                try
                {
                    var context = new VideoDistributionDataContext();
                    var results = context.LostPasswordAdmin(model.Email).FirstOrDefault();

                    if (results?.Result != "NotExists")
                    {
                        string mailbody = "Hello {0} {1},<br /><br />";
                        mailbody += "Your password : {2} <br /> <a href='http://admin.videodistribution.net/Account/Login' target='_blank'>Login</a><br><br>Thank you,<br>VideoDistribution<br>&copy; 2011-2017";
                        mailbody = String.Format(mailbody, Utils.Utils.safeStr(results.Name), Utils.Utils.safeStr(results.Lastname), Utils.Utils.safeStr(results.Password));
                        Utils.Utils.gmailEmailServer(EmailConfigEnum.NOREPLY, Utils.Utils.safeStr(results.Email), "VideoDistribution Lostpassword <gd@VideoDistribution.com>", "Password Notification", mailbody);

                        ViewBag.message = "Email has been sent";
                        return View("ForgotPassword");
                    }
                    else
                    {
                        ViewBag.message = "User Not Found";
                        return View("ForgotPassword");
                    }
                }
                catch
                {
                    return View("~/Views/Login/ActivateUser");
                }
            }
            else
            {
                return View();
            }
        }

        [AllowAnonymous]
        public ActionResult ForgotPasswordConfirmation()
        {
            return View();
        }

        [AllowAnonymous]
        public ActionResult ResetPasswordConfirmation()
        {
            return View();
        }

        [Authorize(Roles = "SuperAdmin")]
        public ViewResult AdminUsers()
        {
            return View();
        }

        [Authorize(Roles = "SuperAdmin")]
        public ViewResult Accounts()
        {
            return View();
        }

        [Authorize(Roles = "SuperAdmin")]
        public ViewResult AccountWebsites()
        {
            return View();
        }

        [Authorize(Roles = "SuperAdmin")]
        public ViewResult PublisherDomains()
        {
            return View();
        }

        [HttpGet]
        public ActionResult Edit(int id)
        {
            using (var db = new VideoDistributionDataContext())
            {
                var result = db.ADM_AdminsById(id).FirstOrDefault();
                ViewBag.Roles = new SelectList(db.ADM_GetAdminRoles().ToList(), "RoleId", "RoleName", result.RoleId);
                return View(result);
            }
        }

        [HttpPost]
        public ActionResult Edit(ADM_AdminsByIdResult result)
        {
            var db = new VideoDistributionDataContext();
            db.ADM_UpdateAdmins(
                result.Id,
                result.Name,
                result.Lastname,
                result.Email,
                result.Active,
                Convert.ToInt32(result.Roles)
            );
            return RedirectToAction("AdminUsers");
        }

        [HttpGet]
        public ActionResult ActivateAccount(int id)
        {
            #region 20171018 - kerim.cem
            //string email = Request.QueryString["email"];
            //using (var db = new VideoDistributionDataContext())
            //{
            //    var result = db.ADM_ActivateAdmins(email).FirstOrDefault().Column1.ToString();

            //    if (result == "Active")
            //    {
            //        ViewBag.Activation = "Account was already activated";
            //        return View("Login");
            //    }
            //    else if (result == "Updated")
            //    {
            //        ViewBag.Activation = "Account activated";
            //        return View("Login");
            //    }
            //    else
            //    {
            //        ViewBag.Activation = "Account not found";
            //        return View("Login");
            //    }
            //} 
            #endregion

            using (var db = new VideoDistributionDataContext())
            {
                var result = db.GetAccountsById(id).FirstOrDefault();
                return View(result);
            }
        }

        public JsonResult Delete(int id)
        {
            using (var db = new VideoDistributionDataContext())
            {
                var result = db.ADM_DeleteAdmin(id);
                var jsonResult = Json(result, JsonRequestBehavior.AllowGet);
                return jsonResult;
            }
        }

        public JsonResult GetAdminRoles()
        {
            using (var db = new VideoDistributionDataContext())
            {
                var result = db.ADM_GetAdminRoles().ToList();
                var jsonResult = Json(result, JsonRequestBehavior.AllowGet);
                return jsonResult;
            }
        }

        public JsonResult GetAdmins(int pagesize, int pagenum)
        {
            using (var db = new VideoDistributionDataContext())
            {
                var filtersCount = int.Parse(Request.QueryString.GetValues("filterscount")[0]);
                if (filtersCount > 0)
                {
                    for (int i = 0; i < filtersCount; i++)
                    {
                        var filterValue = Request.QueryString.GetValues("filtervalue" + i)[0];
                        var filterCondition = Request.QueryString.GetValues("filtercondition" + i)[0];
                        var filterDataField = Request.QueryString.GetValues("filterdatafield" + i)[0];
                        var filterOperator = Request.QueryString.GetValues("filteroperator" + i)[0];
                        int? totalAdmins = 0;
                        var result = db.ADM_GetAdmins(pagesize, pagenum, filterDataField, filterValue, "Id", "", ref totalAdmins).ToList();

                        var finalresult = new
                        {
                            TotalRows = result.Count(),
                            Rows = result.Skip(pagesize * pagenum).Take(pagesize)
                        };
                        var jsonResult = Json(finalresult, JsonRequestBehavior.AllowGet);
                        jsonResult.MaxJsonLength = int.MaxValue;
                        return jsonResult;
                    }
                    var finalresult1 = new
                    {
                        TotalRows = 12,
                        Rows = 5
                    };
                    var jsonResult1 = Json(finalresult1, JsonRequestBehavior.AllowGet);
                    jsonResult1.MaxJsonLength = int.MaxValue;
                    return jsonResult1;
                }
                else
                {
                    int? totalAdmins = 0;
                    var result = db.ADM_GetAdmins(pagesize, pagenum, "", "", "title", "", ref totalAdmins).ToList();
                    var finalresult = new
                    {
                        TotalRows = totalAdmins,
                        Rows = result
                    };
                    var jsonResult = Json(finalresult, JsonRequestBehavior.AllowGet);
                    jsonResult.MaxJsonLength = int.MaxValue;
                    return jsonResult;
                }
            }
        }

        public JsonResult GetUsers(string sortdatafield, string sortorder, int pagesize, int pagenum)
        {
            using (var db = new VideoDistributionDataContext())
            {
                var query = Request.QueryString;
                var dbResult = db.ExecuteQuery<view_ADM_User>(Utils.Utils.BuildQuery(query, "view_ADM_Users")).OrderByDescending(u => u.RegDate).ToList();
                var users = from user in dbResult
                            select new view_ADM_User
                            {
                                Id = user.Id,
                                Password = user.Password,
                                Name = user.Name,
                                Lastname = user.Lastname,
                                Email = user.Email,
                                CompanyName = user.CompanyName,
                                WebsiteURL = user.WebsiteURL,
                                ServerName = user.ServerName,
                                Active = user.Active,
                                LastLoginIP = user.LastLoginIP
                            };
                var total = dbResult.Count();
                users = users.Skip(pagesize * pagenum).Take(pagesize);
                var result = new
                {
                    TotalRows = total,
                    Rows = users
                };
                return Json(result, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult GetAccounts(int pagesize, int pagenum)
        {
            int? totalAccounts = 0;
            using (var db = new VideoDistributionDataContext())
            {
                var filtersCount = int.Parse(Request.QueryString.GetValues("filterscount")[0]);
                if (filtersCount > 0)
                {
                    for (int i = 0; i < filtersCount; i++)
                    {
                        var filterValue = Request.QueryString.GetValues("filtervalue" + i)[0];
                        var filterCondition = Request.QueryString.GetValues("filtercondition" + i)[0];
                        var filterDataField = Request.QueryString.GetValues("filterdatafield" + i)[0];
                        var filterOperator = Request.QueryString.GetValues("filteroperator" + i)[0];
                        var result = db.GetAccounts(pagesize, pagenum, filterDataField, filterValue, this.SortDataField, this.SortOrder, ref totalAccounts).ToList();

                        var finalresult = new
                        {
                            TotalRows = result.Count(),
                            Rows = result.Skip(pagesize * pagenum).Take(pagesize)
                        };
                        var jsonResult = Json(finalresult, JsonRequestBehavior.AllowGet);
                        jsonResult.MaxJsonLength = int.MaxValue;
                        return jsonResult;
                    }
                    var finalresult1 = new
                    {
                        TotalRows = 12,
                        Rows = 5
                    };
                    var jsonResult1 = Json(finalresult1, JsonRequestBehavior.AllowGet);
                    jsonResult1.MaxJsonLength = int.MaxValue;
                    return jsonResult1;
                }
                else
                {
                    var result = db.GetAccounts(pagesize, pagenum, "", "", this.SortDataField, this.SortOrder, ref totalAccounts).ToList();
                    var finalresult = new
                    {
                        TotalRows = totalAccounts,
                        Rows = result
                    };
                    var jsonResult = Json(finalresult, JsonRequestBehavior.AllowGet);
                    jsonResult.MaxJsonLength = int.MaxValue;
                    return jsonResult;
                }
            }
        }

        public JsonResult GetPublisherDomains(string sortdatafield, string sortorder, int pagesize, int pagenum)
        {
            using (VideoDistributionDataContext db = new VideoDistributionDataContext())
            {
                var query = Request.QueryString;
                var dbResult = db.ExecuteQuery<view_ADM_PublisherDomain>(Utils.Utils.BuildQuery(query, "view_ADM_PublisherDomains")).OrderByDescending(g => g.AddedDate).ToList();
                var domains = from domain in dbResult
                             select new view_ADM_PublisherDomain
                             {
                                 Id = domain.Id,
                                 Domain = domain.Domain,
                                 Title = domain.Title,
                                 AdsEnabled = domain.AdsEnabled
                             };
                var total = dbResult.Count();
                domains = domains.Skip(pagesize * pagenum).Take(pagesize);
                if (this.SortDataField != null && this.SortOrder != "")
                {
                    domains = Utils.Utils.SortOrders<view_ADM_PublisherDomain>(domains, this.SortDataField, this.SortOrder);
                }
                var result = new
                {
                    TotalRows = total,
                    Rows = domains
                };
                return Json(result, JsonRequestBehavior.AllowGet);
            }
        }

        public void ChangeAdsEnabled(int id, bool state)
        {
            try
            {
                using (var db = new VideoDistributionDataContext())
                {
                    db.ADM_ChangeAdsEnabled(id, state);
                }
            }
            catch (Exception ex)
            {

            }
        }

        public JsonResult Activate(int id, string companyName, string firstName, string lastName, string email, string password, string revenueShareWalkthrough)
        {
            StringBuilder result = new StringBuilder();
            string partnerId = "";
            string developerId = "";
            string userId = "";
            bool sendEmail = true;

            try
            {
                if (id < 1)
                {
                    result.AppendLine("AccountId can't be smaller than 1.");
                    return Json(new { result = result.ToString() });
                }

                if (string.IsNullOrEmpty(companyName))
                {
                    result.AppendLine("Company name can't be empty string.");
                    return Json(new { result = result.ToString() });
                }

                if (string.IsNullOrEmpty(firstName))
                {
                    result.AppendLine("First name can't be empty string.");
                    return Json(new { result = result.ToString() });
                }

                if (string.IsNullOrEmpty(lastName))
                {
                    result.AppendLine("Last name can't be empty string.");
                    return Json(new { result = result.ToString() });
                }

                if (string.IsNullOrEmpty(email))
                {
                    result.AppendLine("Email can't be empty string.");
                    return Json(new { result = result.ToString() });
                }

                using (var db = new VideoDistributionDataContext())
                {
                    db.AddDeveloperVideoDistribution(ref partnerId, ref developerId, ref userId, companyName, firstName, lastName, email, password);
                }

                if (!string.IsNullOrEmpty(partnerId))
                {
                    result.AppendLine("1. Activation in old report system is successful.");
                    var deserializedResponse = CreatePartner2(companyName, firstName, lastName, email, password, partnerId);
                    if (deserializedResponse != null)
                    {
                        if (deserializedResponse.ResponseCode == 100)
                        {
                            result.AppendLine("2. Partner is created in new report system.");
                            //deserializedResponse = CreateUser(firstName, lastName, email, password, partnerId);

                            //if (deserializedResponse != null)
                            //{
                            //    if (deserializedResponse.ResponseCode == 100)
                            //    {
                            //        result.AppendLine("3. User is created in new report system.");
                            //result.AppendLine("ACTIVATION IS SUCCESSFUL!!!");
                            //}
                            //else if (deserializedResponse.ResponseCode == 500)
                            //{
                            //    result.AppendLine("3. User is already created before in new report system.");
                            //    result.AppendLine("ACTIVATION IS SUCCESSFUL!!!");
                            //}
                            //else
                            //{
                            //    result.AppendLine("3. Error occurred when creating user in new report system.");
                            //    Deactivate(id);
                            //    sendEmail = false;
                            //    result.AppendLine("ACTIVATION HAS ERROR!!!");
                            //}
                            //}
                        }
                        else if (deserializedResponse.ResponseCode == 3002)
                        {
                            result.AppendLine("2. Partner is already created before in new report system.");
                            //deserializedResponse = CreateUser(firstName, lastName, email, password, partnerId);

                            //if (deserializedResponse != null)
                            //{
                            //    if (deserializedResponse.ResponseCode == 100)
                            //    {
                            //        result.AppendLine("3. User is created in new report system.");
                            //result.AppendLine("ACTIVATION IS SUCCESSFUL!!!");
                            //    }
                            //    else if (deserializedResponse.ResponseCode == 500)
                            //    {
                            //        result.AppendLine("3. User is already created before in new report system.");
                            //        result.AppendLine("ACTIVATION IS SUCCESSFUL!!!");
                            //    }
                            //    else
                            //    {
                            //        result.AppendLine("3. Error occurred when creating user in new report system.");
                            //        Deactivate(id);
                            //        sendEmail = false;
                            //        result.AppendLine("ACTIVATION HAS ERROR!!!");
                            //    }
                            //}
                        }
                        else
                        {
                            result.AppendLine("2. Error occurred when creating partner in new report system.");
                            //Deactivate(id);
                            sendEmail = false;
                            result.AppendLine("ACTIVATION HAS ERROR!!!");
                        }
                    }
                    else
                    {
                        result.AppendLine("2. Error occurred when creating partner in new report system.");
                        //Deactivate(id);
                        sendEmail = false;
                        result.AppendLine("ACTIVATION HAS ERROR!!!");
                        Utils.Utils.WriteErrorLog(result.ToString());
                        return Json(new { result = result.ToString() });
                    }
                }
                else
                {
                    result.AppendLine("1. Activation in old report system is failed.");
                    result.AppendLine("ACTIVATION HAS ERROR!!!");
                    Utils.Utils.WriteErrorLog(result.ToString());
                    return Json(new { result = result.ToString() });
                }

                using (var db = new VideoDistributionDataContext())
                {
                    if (!string.IsNullOrEmpty(partnerId) && !string.IsNullOrEmpty(developerId) && !string.IsNullOrEmpty(userId))
                    {
                        db.ActivateAccount(id, partnerId, developerId, userId).FirstOrDefault();
                        result.AppendLine("3. Activation in Video Distribution is successful.");
                        result.AppendLine("ACTIVATION IS SUCCESSFUL!!!");
                    }
                    else
                    {
                        result.AppendLine("3. Activation in old report system is failed.");
                        result.AppendLine("ACTIVATION HAS ERROR!!!");
                        Utils.Utils.WriteErrorLog(result.ToString());
                        return Json(new { result = result.ToString() });
                    }
                }
            }
            catch (Exception ex)
            {
                Deactivate(id);
                sendEmail = false;
                result.AppendLine("ACTIVATION HAS ERROR!!!");
                result.AppendLine(ex.Message);
            }

            if (sendEmail)
            {
                string mailbody = "Hello {0} {1},<br /><br />";
                mailbody += "Your account has been activated. <br />";
                mailbody += "Your password : {2} <br /> <a href='http://videodistribution.com/Login' target='_blank'>Login</a><br><br>Thank you,<br>VideoDistribution<br>&copy; 2011-2017";
                mailbody = String.Format(mailbody, Utils.Utils.safeStr(firstName), Utils.Utils.safeStr(lastName), Utils.Utils.safeStr(password));
                Utils.Utils.gmailEmailServer(EmailConfigEnum.INFO, Utils.Utils.safeStr(email), firstName + " " + lastName, "Account Activation", mailbody);
            }

            Utils.Utils.WriteErrorLog(result.ToString());
            return Json(new { result = result.ToString() });
        }

        public JsonResult Deactivate(int id, bool deactivateUsers = false)
        {
            using (var db = new VideoDistributionDataContext())
            {
                var result = db.DeactivateAccount(id, deactivateUsers).FirstOrDefault();
                var jsonResult = Json(result, JsonRequestBehavior.AllowGet);
                return jsonResult;
            }
        }

        public JsonResult DeactivateAccount(int id)
        {
            return Deactivate(id, true);
        }

        public JsonResult GetAccountWebsites(int pagesize, int pagenum)
        {
            int? totalAccountWebsites = 0;
            using (var db = new VideoDistributionDataContext())
            {
                var filtersCount = int.Parse(Request.QueryString.GetValues("filterscount")[0]);
                if (filtersCount > 0)
                {
                    for (int i = 0; i < filtersCount; i++)
                    {
                        var filterValue = Request.QueryString.GetValues("filtervalue" + i)[0];
                        var filterCondition = Request.QueryString.GetValues("filtercondition" + i)[0];
                        var filterDataField = Request.QueryString.GetValues("filterdatafield" + i)[0];
                        var filterOperator = Request.QueryString.GetValues("filteroperator" + i)[0];
                        var result = db.GetAccountWebsites(pagesize, pagenum, filterDataField, filterValue, this.SortDataField, this.SortOrder, ref totalAccountWebsites).ToList();

                        var finalresult = new
                        {
                            TotalRows = result.Count(),
                            Rows = result.Skip(pagesize * pagenum).Take(pagesize)
                        };
                        var jsonResult = Json(finalresult, JsonRequestBehavior.AllowGet);
                        jsonResult.MaxJsonLength = int.MaxValue;
                        return jsonResult;
                    }
                    var finalresult1 = new
                    {
                        TotalRows = 12,
                        Rows = 5
                    };
                    var jsonResult1 = Json(finalresult1, JsonRequestBehavior.AllowGet);
                    jsonResult1.MaxJsonLength = int.MaxValue;
                    return jsonResult1;
                }
                else
                {
                    var result = db.GetAccountWebsites(pagesize, pagenum, "", "", this.SortDataField, this.SortOrder, ref totalAccountWebsites).ToList();
                    var finalresult = new
                    {
                        TotalRows = totalAccountWebsites,
                        Rows = result
                    };
                    var jsonResult = Json(finalresult, JsonRequestBehavior.AllowGet);
                    jsonResult.MaxJsonLength = int.MaxValue;
                    return jsonResult;
                }
            }
        }

        public JsonResult ActivateAccountWebsite(int id, string partnerId, string url, string accountName)
        {
            StringBuilder result = new StringBuilder();
            string portalId = "";
            string affiliateId = "";
            bool sendEmail = true;

            try
            {
                if (id < 1)
                {
                    result.AppendLine("AccountWebsiteId can't be smaller than 1.");
                    return Json(new { result = result.ToString() });
                }

                if (string.IsNullOrEmpty(partnerId))
                {
                    result.AppendLine("Account hasn't have a PartnerId. You should be activate the account firstly.");
                    return Json(new { result = result.ToString() });
                }

                if (string.IsNullOrEmpty(url))
                {
                    result.AppendLine("Domain can't be empty string.");
                    return Json(new { result = result.ToString() });
                }

                if (string.IsNullOrEmpty(accountName))
                {
                    result.AppendLine("AccountName can't be empty string.");
                    return Json(new { result = result.ToString() });
                }

                using (var db = new VideoDistributionDataContext())
                {
                    db.AddPortal(ref portalId, partnerId, ref affiliateId, url, url);

                    if (!string.IsNullOrEmpty(portalId) && !string.IsNullOrEmpty(affiliateId))
                    {
                        result.AppendLine("1. Portal is created in old report system.");

                        var deserializedResponse = CreatePlatform(url, partnerId);
                        if (deserializedResponse != null)
                        {
                            if (deserializedResponse.ResponseCode == 100)
                            {
                                result.AppendLine("2. Domain is created in new report system.");
                                db.ActivateAccountWebsite(id, url.Replace("https://", "").Replace("http://", "").Split('/')[0], accountName, 
                                                          affiliateId, portalId, partnerId.Replace("-", ""));
                                result.AppendLine("3. Website is activated in Video Distribution.");
                                result.AppendLine("ACTIVATION IS SUCCESSFUL!!!");
                            }
                            else if (deserializedResponse.ResponseCode == 3007)
                            {
                                result.AppendLine("2. Doamin is already created before in new report system.");
                                db.ActivateAccountWebsite(id, url.Replace("https://", "").Replace("http://", "").Split('/')[0], accountName, 
                                                          affiliateId, portalId, partnerId.Replace("-", ""));
                                result.AppendLine("3. Website is activated in Video Distribution.");
                                result.AppendLine("ACTIVATION IS SUCCESSFUL!!!");
                            }
                            else
                            {
                                result.AppendLine("2. Error occurred when creating domain in new report system.");
                                sendEmail = false;
                                result.AppendLine("ACTIVATION HAS ERROR!!!");
                            }
                        }
                        else
                        {
                            result.AppendLine("2. Error occurred when creating domain in new report system.");
                            sendEmail = false;
                            result.AppendLine("ACTIVATION HAS ERROR!!!");
                        }
                    }
                    else
                    {
                        result.AppendLine("1. Old report system did not send PortalId or AffiliateId or both of them.");
                        result.AppendLine("ACTIVATION HAS ERROR!!!");
                        sendEmail = false;
                    }
                }
            }
            catch (Exception ex)
            {
                result.AppendLine("ACTIVATION HAS ERROR!!!");
                result.AppendLine(ex.Message);
                sendEmail = false;
            }

            if (sendEmail)
            {
                ADM_GetUserByAccountWebsiteIdResult user = null;
                using (var db = new VideoDistributionDataContext())
                {
                    user = db.ADM_GetUserByAccountWebsiteId(id).FirstOrDefault();
                }

                if (user != null)
                {
                    string mailbody = "Hello {0} {1},<br /><br />";
                    mailbody += "Your website in the below has been activated. <br />";
                    mailbody += "{2}<br /><br />";
                    mailbody += "Thank you,<br>VideoDistribution<br>&copy; 2011-2017";
                    mailbody = String.Format(mailbody, Utils.Utils.safeStr(user.Name), Utils.Utils.safeStr(user.Lastname), Utils.Utils.safeStr(url));
                    Utils.Utils.gmailEmailServer(EmailConfigEnum.INFO, Utils.Utils.safeStr(user.Email), user.Name + " " + user.Lastname, "Website Activation", mailbody);
                }
            }

            Utils.Utils.WriteErrorLog(result.ToString());
            return Json(new { result = result.ToString() });
        }

        [HttpGet]
        public ViewResult CreateAdmin()
        {
            using (var db = new VideoDistributionDataContext())
            {
                IEnumerable<SelectListItem> roles = db.ADM_GetAdminRoles().Select(r => new SelectListItem
                {
                    Value = r.RoleId.ToString(),
                    Text = r.RoleName
                }).ToList();

                ViewBag.Roles = roles;
            }
            return View();
        }

        [HttpPost]
        public ActionResult CreateAdmin(AdminViewModel result)
        {
            if (ModelState.IsValid)
            {
                using (var db = new VideoDistributionDataContext())
                {
                    db.ADM_CreateAdmins(result.Email, result.Name, result.Lastname, result.RoleId, false);
                    var password = db.GetUsers().ToList().Where(u => u.Email == result.Email).FirstOrDefault().Password;
                    string mailbody = "Hello {0} {1},<br /><br />";
                    mailbody += "{2} added you as a user<br />";
                    mailbody += "Your password: {3} <br /> <a href='http://admin.videodistribution.net/Account/ActivateAccount/?email=" + result.Email + "' target='_blank'>Activate your account</a><br><br>Thank you,<br />VideoDistribution<br />&copy; 2011-2017";
                    mailbody = String.Format(mailbody, Utils.Utils.safeStr(result.Name), Utils.Utils.safeStr(result.Lastname), User.Identity.GetUserName(), Utils.Utils.safeStr(password), Utils.Utils.safeStr(result.Password));
                    Utils.Utils.gmailEmailServer(EmailConfigEnum.ADMIN, Utils.Utils.safeStr(result.Email), result.Name + " " + result.Lastname, "Password Notification", mailbody);
                }
                return RedirectToAction("AdminUsers");
            }
            else
            {
                return View("CreateAdmin");
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (_userManager != null)
                {
                    _userManager.Dispose();
                    _userManager = null;
                }

                if (_signInManager != null)
                {
                    _signInManager.Dispose();
                    _signInManager = null;
                }
            }

            base.Dispose(disposing);
        }

        #region Helpers
        // Used for XSRF protection when adding external logins
        private const string XsrfKey = "XsrfId";

        private IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError("", error);
            }
        }

        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            return RedirectToAction("Index", "Home");
        }

        private TunnlResponse CreatePartner(string partnerId, string companyName, string email)
        {
            //Create Partner
            //20171023 - kerim.cem - RevenueShareWalkthrough gönderilmeyecek.
            using (var http = new HttpClient())
            {
                string createPartnerRequest = @"{
                                                        ""Session"" :{
                                                        ""Token"":""1356b0be-8156-4dae-97d4-4d573d6c3b88""
                                                        },
                                                        ""Data"" :{
                                                        ""Id"":'" + partnerId + @"',
                                                        ""Name"":'" + companyName + @"',
                                                        ""Contact"":'" + email + @"',
                                                        ""Comment"":"""",
                                                        ""UseNetCpm"":"""",
                                                        ""RevenueShareGame"":"""",
                                                        ""RevenueShareWalkthrough"":"""",
                                                        ""RevenueSharePagePreroll"":""""
                                                        }
                                                    }";
                var content = new StringContent(createPartnerRequest);
                content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json");
                var request = http.PostAsync("http://api.tunnl.com/Partner/CreatePartner", content);
                var response = request.Result.Content.ReadAsStringAsync().Result;
                return new JavaScriptSerializer().Deserialize<TunnlResponse>(response);
            }
        }

        private TunnlResponse CreateUser(string firstName, string lastName, string email, string password, string partnerId)
        {
            //Create User
            using (var http = new HttpClient())
            {
                string createUserRequest = @"{
                                                    ""Session"" :{
                                                    ""Token"":""1356b0be-8156-4dae-97d4-4d573d6c3b88""
                                                    },
                                                    ""Data"" :{
                                                    ""FirstName"":'" + firstName + @"',
                                                    ""LastName"":'" + lastName + @"',
                                                    ""EMail"":'" + email + @"',
                                                    ""Password"":'" + password + @"',
                                                    ""PartnerId"":'" + partnerId + @"'
                                                    }
                                                 }";
                var contentUser = new StringContent(createUserRequest);
                contentUser.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json");
                var request = http.PostAsync("http://api.tunnl.com/User/CreateUser", contentUser);
                var response = request.Result.Content.ReadAsStringAsync().Result;
                return new JavaScriptSerializer().Deserialize<TunnlResponse>(response);
            }
        }

        private TunnlResponse CreatePartner2(string companyName, string firstName, string lastName, string email, string password, string partnerId)
        {
            //Create Partner
            using (var http = new HttpClient())
            {
                string createPartnerRequest = @"{
                                                    ""Session"" :{
                                                    ""Token"":""1356b0be-8156-4dae-97d4-4d573d6c3b88""
                                                    },
                                                    ""Data"" :{
                                                    ""PartnerTitle"":'" + companyName + @"',
                                                    ""Id"":'" + partnerId + @"',
                                                    ""FirstName"":'" + firstName + @"',
                                                    ""LastName"":'" + lastName + @"',
                                                    ""Email"":'" + email + @"',
                                                    ""UserPassword"":'" + password + @"',
                                                    ""UseNetCpm"": false
                                                    }
                                                 }";
                var contentUser = new StringContent(createPartnerRequest);
                contentUser.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json");
                var request = http.PostAsync("http://api.tunnl.com:8080/Partner/CreatePartner", contentUser);
                var response = request.Result.Content.ReadAsStringAsync().Result;
                return new JavaScriptSerializer().Deserialize<TunnlResponse>(response);
            }
        }

        private TunnlResponse CreatePlatform(string url, string partnerId)
        {
            //Create Partner
            using (var http = new HttpClient())
            {
                string createPlatformRequest = @"{
                                                    ""Session"" :{
                                                    ""Token"":""1356b0be-8156-4dae-97d4-4d573d6c3b88""
                                                    },
                                                    ""Data"" :{
                                                    ""PartnerId"":'" + partnerId + @"',
                                                    ""Url"":'" + url + @"',
                                                    ""AdTagType"": 2
                                                    }
                                                 }";
                var contentUser = new StringContent(createPlatformRequest);
                contentUser.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json");
                var request = http.PostAsync("http://api.tunnl.com:8080/Platform/CreatePlatform", contentUser);
                var response = request.Result.Content.ReadAsStringAsync().Result;
                return new JavaScriptSerializer().Deserialize<TunnlResponse>(response);
            }
        }


        internal class ChallengeResult : HttpUnauthorizedResult
        {
            public ChallengeResult(string provider, string redirectUri)
                : this(provider, redirectUri, null)
            {
            }

            public ChallengeResult(string provider, string redirectUri, string userId)
            {
                LoginProvider = provider;
                RedirectUri = redirectUri;
                UserId = userId;
            }

            public string LoginProvider { get; set; }
            public string RedirectUri { get; set; }
            public string UserId { get; set; }

            public override void ExecuteResult(ControllerContext context)
            {
                var properties = new AuthenticationProperties { RedirectUri = RedirectUri };
                if (UserId != null)
                {
                    properties.Dictionary[XsrfKey] = UserId;
                }
                context.HttpContext.GetOwinContext().Authentication.Challenge(properties, LoginProvider);
            }
        }
        #endregion
    }
}