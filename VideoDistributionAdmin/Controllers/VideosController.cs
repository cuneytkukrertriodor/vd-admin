﻿using VideoDistribution.Models;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web.Mvc;

namespace VideoDistribution.Controllers
{
    [Authorize(Roles = "SuperAdmin,Admin")]
    public class VideosController : Controller
    {
        public string SortDataField
        {
            get
            {
                var sortDataField = "Id";
                if (Request.QueryString.GetValues("sortdatafield") != null && Request.QueryString.GetValues("sortdatafield").ToList().Count > 0)
                    sortDataField = Request.QueryString.GetValues("sortdatafield")[0];
                return sortDataField;
            }
        }

        public string SortOrder
        {
            get
            {
                var sortOrder = "desc";
                if (Request.QueryString.GetValues("sortorder") != null && Request.QueryString.GetValues("sortorder").ToList().Count > 0)
                    sortOrder = Request.QueryString.GetValues("sortorder")[0].ToUpper();
                return sortOrder;
            }
        }

        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public ActionResult Edit(string id,string type)
        {
            var db = new VideoDistributionDataContext();
            var result = db.ADM_VideoById(id).FirstOrDefault();
            ViewBag.VideoMd5 = id;
            ViewBag.Category = new SelectList(db.GetCategories(), "Id", "Title", result.CategoryId);
            ViewBag.Company = new SelectList(db.STAT_getUsersList("s1").OrderBy(u => u.CompanyName), "Id", "CompanyName", result.UserId);
            ViewBag.Privacy = new SelectList(db.GetVideoPrivacy(), "Id", "Title", result.PrivacyId);
            var visibility = new List<int>() { 0, 1 };
            ViewBag.Visible = new SelectList(visibility, result.Visible);
            ViewBag.type = type;
            ViewBag.Title = result.Title;
            //ViewBag.GameTypes = new SelectList(db.GetGameTypes(), "Id", "TypeName", result.GameType);
            return View(result);
        }

        public JsonResult UpdateVideosInline(view_ADM_Video result)
        {
            using (var db = new VideoDistributionDataContext())
            {
                db.UpdateVideo(null,result.VideoMd5,result.Title,Convert.ToBoolean(result.Approved),Convert.ToBoolean(result.Visible),Convert.ToBoolean(result.Banner),
                    null,null,null,null,null,null,null,null,null
                );
                return Json(true, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult Edit(ADM_VideoByIdResult result)
        {
            var db = new VideoDistributionDataContext();
            db.UpdateVideo(
                result.UserId, 
                result.VideoMd5, 
                result.Title, 
                null, 
                Convert.ToBoolean(result.Visible), 
                Convert.ToBoolean(null), 
                result.Warning, 
                result.VideoType, 
                result.Width, 
                result.Height, 
                result.ExternalURL, 
                result.Description, 
                result.Filename, 
                Convert.ToInt32(result.Category),
                Convert.ToInt32(result.Privacy)
            );
            return RedirectToAction("Index");
        }

        public JsonResult GetVideos(string sortdatafield, string sortorder, int pagesize, int pagenum)
        {
            using (VideoDistributionDataContext db = new VideoDistributionDataContext())
            {
                var query = Request.QueryString;
                var dbResult = db.ExecuteQuery<view_ADM_Video>(Utils.Utils.BuildQuery(query, "view_ADM_Videos")).ToList();
                var videos = from video in dbResult
                            select new view_ADM_Video
                            {
                                Id = video.Id,
                                VideoMd5 = video.VideoMd5,
                                Title = video.Title,
                                Warning = video.Warning,
                                Approved = video.Approved,
                                Visible = video.Visible,
                                Banner = video.Banner,
                                AddedDate = video.AddedDate,
                                CompanyName  = video.CompanyName,
                                UserId = video.UserId,
                                VideoType = video.VideoType,
                                ProviderId = video.ProviderId,
                                ProviderName = video.ProviderName
                            };


                var total = dbResult.Count();
                if (this.SortDataField != null && this.SortOrder != "")
                {
                    videos = Utils.Utils.SortOrders<view_ADM_Video>(videos, this.SortDataField, this.SortOrder);
                }
                videos = videos.Skip(pagesize * pagenum).Take(pagesize);
                var result = new
                {
                    TotalRows = total,
                    Rows = videos
                };
               return Json(result, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult Feature(int id)
        {
            var db = new VideoDistributionDataContext();
            var result = db.APP_InsertFeaturedVideos(id.ToString());
            return RedirectToAction("Index", "FeaturedVideos");
        }

        public ActionResult Report()
        {
            return View();
        }

        [Authorize(Roles = "Superadmin")]
        public JsonResult GetVideosByCategoryAndAccount()
        {
            using (VideoDistributionDataContext db = new VideoDistributionDataContext())
            {
                var result = db.ADM_VideosByCategoryAndAccount().ToList();
                var categoryResult = result.GroupBy(r => r.CatTitle)
                                           .Select(group => new { Category = group.Key, Count = group.Sum(x => x.VideoCount)})
                                           .OrderBy(y => y.Category);
                var accountResult = result.GroupBy(r => r.AccountName)
                                           .Select(group => new { Account = group.Key, Count = group.Sum(x => x.VideoCount) })
                                           .OrderBy(y => y.Account);
                var finalresult = new
                {
                    Category = categoryResult,
                    Account = accountResult
                };
                return Json(finalresult, JsonRequestBehavior.AllowGet);
            }
        }
    }
}