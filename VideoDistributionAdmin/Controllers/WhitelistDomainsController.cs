﻿using VideoDistribution.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using System.Xml.Linq;

namespace VideoDistribution.Controllers
{
    [Authorize(Roles = "SuperAdmin,Admin")]
    public class WhitelistDomainsController : Controller
    {
        // GET: WhitelistDomains
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Delete(int id)
        {
            var db = new VideoDistributionDataContext();
            var result = db.ADM_Delete_WhiteListDomain(id);
            return View("Index");
        }

        public JsonResult GetWhiteListDomains(string sortdatafield, string sortorder, int pagesize, int pagenum)
        {
            using (var db = new VideoDistributionDataContext())
            {
                var query = Request.QueryString;
                var dbResult = db.ExecuteQuery<view_WhitelistDomain>(Utils.Utils.BuildQuery(query, "view_WhitelistDomains")).OrderByDescending(g => g.AdRequest).ToList();
                var whiteListDomains = from domain in dbResult
                            select new view_WhitelistDomain
                            {
                                Id = domain.Id,
                                AdNetworkId = domain.AdNetworkId,
                                Domain = domain.Domain,
                                AddedDate = domain.AddedDate,
                                HtmlBanner = domain.HtmlBanner,
                                FamobiId = domain.FamobiId,
                                Active = domain.Active,
                                Session = domain.Session,
                                AdRequest = domain.AdRequest,
                                R1 = domain.R1
                            };
                var total = dbResult.Count();
                whiteListDomains = whiteListDomains.Skip(pagesize * pagenum).Take(pagesize);
                if (sortdatafield != null && sortorder != "")
                {
                    whiteListDomains = Utils.Utils.SortOrders<view_WhitelistDomain>(whiteListDomains, sortdatafield, sortorder);
                }
                var result = new
                {
                    TotalRows = total,
                    Rows = whiteListDomains
                };
                return Json(result, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult UpdateDomainsInline(view_WhitelistDomain result)
        {
            using (var db = new VideoDistributionDataContext())
            {
                db.ADM_UpdateWhitelistDomains(
                    result.Id,
                    result.HtmlBanner,
                    result.Active,
                    result.FamobiId
                );
                return Json(true, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult AddDomains(string data)
        {
            using (var db = new VideoDistributionDataContext())
            {
                string[] domains = data.Split('\n');

                XDocument xml = new XDocument(new XElement("Domains"));

                foreach (var item in domains)
                {
                    xml.Root.Add(new XElement("Row", new XElement("Domain", item)));
                }

                db.APP_WhiteListDomainAdd(1, xml.Root);

                return Json(data, JsonRequestBehavior.AllowGet);
            }
        }
    }
}