﻿using VideoDistribution.Models;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using static VideoDistribution.Enums.VideoDistAdminEnums;
using Newtonsoft.Json;

namespace VideoDistribution.Controllers
{
    [Authorize(Roles = "SuperAdmin,Admin")]
    public class VideoApproveController : Controller
    {
        public string SortDataField
        {
            get
            {
                var sortDataField = "Id";
                if (Request.QueryString.GetValues("sortdatafield") != null && Request.QueryString.GetValues("sortdatafield").ToList().Count > 0)
                    sortDataField = Request.QueryString.GetValues("sortdatafield")[0];
                return sortDataField;
            }
        }

        public string SortOrder
        {
            get
            {
                var sortOrder = "desc";
                if (Request.QueryString.GetValues("sortorder") != null && Request.QueryString.GetValues("sortorder").ToList().Count > 0)
                    sortOrder = Request.QueryString.GetValues("sortorder")[0].ToUpper();
                return sortOrder;
            }
        }

        public int UserId
        {
            get
            {
                try
                {
                    var cookie = Request.Cookies["userCookie"];
                    var userid = cookie.Values["userid"].ToString();
                    return Convert.ToInt32(userid);
                }
                catch
                {
                    return 0;
                }
            }
            set { }
        }

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult RejectedVideos()
        {
            return View();

        }

        public ActionResult Private()
        {
            return View();

        }
        public ActionResult Deleted()
        {
            return View();

        }

        public ActionResult GetRejectedVideos(string sortdatafield, string sortorder, int pagesize, int pagenum)
        {
            using (VideoDistributionDataContext db = new VideoDistributionDataContext())
            {
                var query = Request.QueryString;
                var dbResult = db.ExecuteQuery<view_ADM_ApprovedVideo>(Utils.Utils.BuildQuery(query, "view_ADM_RejectedVideos")).OrderByDescending(g => g.AddedDate).ToList();
                var videos = from video in dbResult
                             select new view_ADM_ApprovedVideo
                             {
                                 Id = video.Id,
                                 VideoMd5 = video.VideoMd5,
                                 Title = video.Title,
                                 Warning = video.Warning,
                                 Approved = video.Approved,
                                 Visible = video.Visible,
                                 AddedDate = video.AddedDate,
                                 CompanyName = video.CompanyName,
                                 UserId = video.UserId,
                                 VideoType = video.VideoType
                             };
                var total = dbResult.Count();
                videos = videos.Skip(pagesize * pagenum).Take(pagesize);
                if (sortdatafield != null && sortorder != "")
                {
                    videos = Utils.Utils.SortOrders<view_ADM_ApprovedVideo>(videos, sortdatafield, sortorder);
                }
                var result = new
                {
                    TotalRows = total,
                    Rows = videos
                };
                return Json(result, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult GetPrivateVideos(string sortdatafield, string sortorder, int pagesize, int pagenum)
        {
            using (VideoDistributionDataContext db = new VideoDistributionDataContext())
            {
                var query = Request.QueryString;
                var dbResult = db.ExecuteQuery<view_ADM_ApprovedVideo>(Utils.Utils.BuildQuery(query, "view_ADM_PrivateVideos")).OrderByDescending(g => g.AddedDate).ToList();
                var videos = from video in dbResult
                             select new view_ADM_ApprovedVideo
                             {
                                 Id = video.Id,
                                 VideoMd5 = video.VideoMd5,
                                 Title = video.Title,
                                 Warning = video.Warning,
                                 Approved = video.Approved,
                                 Visible = video.Visible,
                                 AddedDate = video.AddedDate,
                                 CompanyName = video.CompanyName,
                                 UserId = video.UserId,
                                 VideoType = video.VideoType,
                                 Watched = video.Watched
                             };
                var total = dbResult.Count();
                videos = videos.Skip(pagesize * pagenum).Take(pagesize);
                if (sortdatafield != null && sortorder != "")
                {
                    videos = Utils.Utils.SortOrders<view_ADM_ApprovedVideo>(videos, sortdatafield, sortorder);
                }
                var result = new
                {
                    TotalRows = total,
                    Rows = videos
                };
                return Json(result, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult GetDeletedVideos(string sortdatafield, string sortorder, int pagesize, int pagenum)
        {
            using (VideoDistributionDataContext db = new VideoDistributionDataContext())
            {
                var query = Request.QueryString;
                var dbResult = db.ExecuteQuery<view_ADM_ApprovedVideo>(Utils.Utils.BuildQuery(query, "view_ADM_DeletedVideos")).OrderByDescending(g => g.AddedDate).ToList();
                var videos = from video in dbResult
                             select new view_ADM_ApprovedVideo
                             {
                                 Id = video.Id,
                                 VideoMd5 = video.VideoMd5,
                                 Title = video.Title,
                                 Warning = video.Warning,
                                 Approved = video.Approved,
                                 Visible = video.Visible,
                                 AddedDate = video.AddedDate,
                                 CompanyName = video.CompanyName,
                                 UserId = video.UserId,
                                 VideoType = video.VideoType
                             };
                var total = dbResult.Count();
                videos = videos.Skip(pagesize * pagenum).Take(pagesize);
                if (sortdatafield != null && sortorder != "")
                {
                    videos = Utils.Utils.SortOrders<view_ADM_ApprovedVideo>(videos, sortdatafield, sortorder);
                }
                var result = new
                {
                    TotalRows = total,
                    Rows = videos
                };
                return Json(result, JsonRequestBehavior.AllowGet);
            }
        }


        public ActionResult GetPublishedVideos(string sortdatafield, string sortorder, int pagesize, int pagenum)
        {
            using (VideoDistributionDataContext db = new VideoDistributionDataContext())
            {
                var query = Request.QueryString;
                var dbResult = db.ExecuteQuery<view_ADM_PublishedVideo>(Utils.Utils.BuildQuery(query, "view_ADM_PublishedVideos")).OrderByDescending(g => g.AddedDate).ToList();
                var videos = from video in dbResult
                             select new view_ADM_ApprovedVideo
                             {
                                 Id = video.Id,
                                 VideoMd5 = video.VideoMd5,
                                 Title = video.Title,
                                 Warning = video.Warning,
                                 Approved = video.Approved,
                                 Visible = video.Visible,
                                 AddedDate = video.AddedDate,
                                 CompanyName = video.CompanyName,
                                 UserId = video.UserId,
                                 VideoType = video.VideoType
                             };
                var total = dbResult.Count();
                videos = videos.Skip(pagesize * pagenum).Take(pagesize);
                if (sortdatafield != null && sortorder != "")
                {
                    videos = Utils.Utils.SortOrders<view_ADM_ApprovedVideo>(videos, sortdatafield, sortorder);
                }
                var result = new
                {
                    TotalRows = total,
                    Rows = videos
                };
                return Json(result, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult PublishedVideos()
        {
            return View();
        }


        private List<TagModel> GetTags(int videoMd5)
        {
            using (var db = new VideoDistributionDataContext())
            {
                var tags = db.GetTagsByVideoId(videoMd5);
                List<TagModel> tagModel = new List<TagModel>();
                foreach (var tag in tags)
                {
                    tagModel.Add(new TagModel
                    {
                        TagId = tag.TagId,
                        Title = tag.Title
                    });
                }
                return tagModel;
            }
        }

        [HttpGet]
        public ActionResult Edit(string id)
        {
            var db = new VideoDistributionDataContext();
            var result = db.ADM_VideoById(id).FirstOrDefault();

            ViewBag.Category = new SelectList(db.GetCategories(), "Id", "Title", result.CategoryId);
            ViewBag.Company = new SelectList(db.STAT_getUsersList("s1").OrderBy(u => u.CompanyName), "Id", "CompanyName", result.UserId);
            //ViewBag.GameTypes = new SelectList(db.GetGameTypes(), "Id", "TypeName", result.GameType);
            return View(result);
        }

        //toDO this method will combine below scripts
        public ActionResult Approve(string videoMd5)
        {
            using (var db = new VideoDistributionDataContext())
            {
                db.ADM_ApproveVideo2(videoMd5, true, UserId);//userid must be added
            }
            return RedirectToAction("Index");
        }

        public ActionResult BulkApprove(string videoMd5List)
        {
            if (!string.IsNullOrEmpty(videoMd5List))
            {
                string[] videoMd5s = JsonConvert.DeserializeObject<string[]>(videoMd5List);
                using (var db = new VideoDistributionDataContext())
                {
                    foreach (string v in videoMd5s)
                    {
                        db.ADM_ApproveVideo2(v, true, UserId);//userid must be added
                    }
                }
            }
            return RedirectToAction("Index");
        }

        public ActionResult ChangeStatus(string videoMd5, string type)
        {
            using (var db = new VideoDistributionDataContext())
            {

                if (VideoStatusEnum.Watched.ToString() == type)
                {
                    db.ADM_VideoWatched(videoMd5, true, UserId);//userid must be added

                }

            }
            return RedirectToAction("Private");
        }

        public ActionResult UnApprove(string videoMd5)
        {
            using (var db = new VideoDistributionDataContext())
            {
                db.ADM_RejectVideo(videoMd5, "", false, UserId);
            }
            return RedirectToAction("Index");
        }

        public ActionResult ActiveteVideo(string videoMd5)
        {
            using (var db = new VideoDistributionDataContext())
            {
                db.ADM_UpdateVideoDeleteStatus(videoMd5, false, "");
            }
            return RedirectToAction("Index");
        }

        public ActionResult DeleteVideo(string videoMd5, string reason, string title)
        {
            using (var db = new VideoDistributionDataContext())
            {
                db.ADM_UpdateVideoDeleteStatus(videoMd5, true, reason);

                ADM_GetUserByVideoMd5Result user = db.ADM_GetUserByVideoMd5(videoMd5).FirstOrDefault();

                if (user != null)
                {
                    string mailbody = "Hello {0} {1},<br /><br />";
                    mailbody += "Your '{2}' titled video was rejected due to the reason: {3} <br /><br />";
                    mailbody += "Thank you,<br>VideoDistribution<br>&copy; 2011-2017";
                    mailbody = String.Format(mailbody, Utils.Utils.safeStr(user.Name), Utils.Utils.safeStr(user.Lastname), Utils.Utils.safeStr(title), Utils.Utils.safeStr(reason));
                    Utils.Utils.gmailEmailServer(EmailConfigEnum.SUPPORT, Utils.Utils.safeStr(user.Email), user.Name + " " + user.Lastname, "Deleted Video", mailbody);
                }
            }
            return RedirectToAction("Index");
        }

        public ActionResult BulkDeleteVideo(string videoMd5List, string reason)
        {
            if (!string.IsNullOrEmpty(videoMd5List))
            {
                var videoMd5s = JsonConvert.DeserializeObject<List<BulkVideoMd5>>(videoMd5List);
                foreach (BulkVideoMd5 bv in videoMd5s)
                {
                    using (var db = new VideoDistributionDataContext())
                    {
                        db.ADM_UpdateVideoDeleteStatus(bv.Id, true, reason);

                        ADM_GetUserByVideoMd5Result user = db.ADM_GetUserByVideoMd5(bv.Id).FirstOrDefault();

                        if (user != null)
                        {
                            string mailbody = "Hello {0} {1},<br /><br />";
                            mailbody += "Your '{2}' titled video was rejected due to the reason: {3} <br /><br />";
                            mailbody += "Thank you,<br>VideoDistribution<br>&copy; 2011-2017";
                            mailbody = String.Format(mailbody, Utils.Utils.safeStr(user.Name), Utils.Utils.safeStr(user.Lastname), Utils.Utils.safeStr(bv.Title), Utils.Utils.safeStr(reason));
                            Utils.Utils.gmailEmailServer(EmailConfigEnum.SUPPORT, Utils.Utils.safeStr(user.Email), user.Name + " " + user.Lastname, "Deleted Video", mailbody);
                        }
                    }
                }
            }
            return RedirectToAction("Index");
        }

        public ActionResult Rejected(string videoMd5, string reason, string title)
        {
            var cookie = Request.Cookies["userCookie"];
            var userid = cookie.Values["userid"].ToString();

            using (var db = new VideoDistributionDataContext())
            {
                db.ADM_RejectVideo(videoMd5, reason, true, UserId);

                ADM_GetUserByVideoMd5Result user = db.ADM_GetUserByVideoMd5(videoMd5).FirstOrDefault();

                if (user != null)
                {
                    string mailbody = "Hello {0} {1},<br /><br />";
                    mailbody += "Your '{2}' titled video was rejected due to the reason: {3} <br /><br />";
                    mailbody += "Thank you,<br>VideoDistribution<br>&copy; 2011-2017";
                    mailbody = String.Format(mailbody, Utils.Utils.safeStr(user.Name), Utils.Utils.safeStr(user.Lastname), Utils.Utils.safeStr(title), Utils.Utils.safeStr(reason));
                    Utils.Utils.gmailEmailServer(EmailConfigEnum.INFO, Utils.Utils.safeStr(user.Email), user.Name + " " + user.Lastname, "Rejected Video", mailbody);
                }
            }
            return RedirectToAction("Index");
        }

        public ActionResult BulkRejected(string videoMd5List, string reason)
        {
            if (!string.IsNullOrEmpty(videoMd5List))
            {
                var videoMd5s = JsonConvert.DeserializeObject<List<BulkVideoMd5>>(videoMd5List);
                var cookie = Request.Cookies["userCookie"];
                var userid = cookie.Values["userid"].ToString();

                foreach (BulkVideoMd5 bv in videoMd5s)
                {
                    using (var db = new VideoDistributionDataContext())
                    {
                        db.ADM_RejectVideo(bv.Id, reason, true, UserId);

                        ADM_GetUserByVideoMd5Result user = db.ADM_GetUserByVideoMd5(bv.Id).FirstOrDefault();

                        if (user != null)
                        {
                            string mailbody = "Hello {0} {1},<br /><br />";
                            mailbody += "Your '{2}' titled video was rejected due to the reason: {3} <br /><br />";
                            mailbody += "Thank you,<br>VideoDistribution<br>&copy; 2011-2017";
                            mailbody = String.Format(mailbody, Utils.Utils.safeStr(user.Name), Utils.Utils.safeStr(user.Lastname), Utils.Utils.safeStr(bv.Title), Utils.Utils.safeStr(reason));
                            Utils.Utils.gmailEmailServer(EmailConfigEnum.INFO, Utils.Utils.safeStr(user.Email), user.Name + " " + user.Lastname, "Rejected Video", mailbody);
                        }
                    }
                }
            }
            return RedirectToAction("Index");
        }

        public JsonResult UpdateVideosInline(view_ADM_Video result)
        {
            using (var db = new VideoDistributionDataContext())
            {
                db.UpdateVideo(null, result.VideoMd5, result.Title, Convert.ToBoolean(result.Approved), Convert.ToBoolean(result.Visible), Convert.ToBoolean(result.Banner),
                    null, null, null, null, null, null, null, null, null
                );
                return Json(true, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult Edit(ADM_VideoByIdResult result)
        {
            var db = new VideoDistributionDataContext();
            db.UpdateVideo(
                result.UserId,
                result.VideoMd5,
                result.Title,
                Convert.ToBoolean(null),
                Convert.ToBoolean(result.Visible),
                Convert.ToBoolean(null),
                result.Warning,
                result.VideoType,
                result.Width,
                result.Height,
                result.ExternalURL,
                result.Description,
                result.Filename,
                Convert.ToInt32(result.Category),
                Convert.ToInt32(result.Privacy)
            );
            return RedirectToAction("Index");
        }


        public ActionResult Details(string id, string title, string addedBy)
        {

            ViewBag.VideoMd5 = id;
            ViewBag.title = title;
            ViewBag.addedBy = addedBy;
            return View();
        }

        public JsonResult GetVideos(string sortdatafield, string sortorder, int pagesize, int pagenum)
        {
            using (VideoDistributionDataContext db = new VideoDistributionDataContext())
            {
                var query = Request.QueryString;
                var dbResult = db.ExecuteQuery<view_ADM_ApprovedVideo>(Utils.Utils.BuildQuery(query, "view_ADM_ApprovedVideos")).OrderByDescending(g => g.AddedDate).ToList();
                var videos = from video in dbResult
                             select new view_ADM_ApprovedVideo
                             {
                                 Id = video.Id,
                                 VideoMd5 = video.VideoMd5,
                                 Title = video.Title,
                                 Warning = video.Warning,
                                 Approved = video.Approved,
                                 Visible = video.Visible,
                                 AddedDate = video.AddedDate,
                                 CompanyName = video.CompanyName,
                                 UserId = video.UserId,
                                 VideoType = video.VideoType
                             };
                var total = dbResult.Count();
                videos = videos.Skip(pagesize * pagenum).Take(pagesize);
                if (this.SortDataField != null && this.SortOrder != "")
                {
                    videos = Utils.Utils.SortOrders<view_ADM_ApprovedVideo>(videos, this.SortDataField, this.SortOrder);
                }
                var result = new
                {
                    TotalRows = total,
                    Rows = videos
                };
                return Json(result, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult Feature(int id)
        {
            var db = new VideoDistributionDataContext();
            var result = db.APP_InsertFeaturedVideos(id.ToString());
            return RedirectToAction("Index", "FeaturedVideos");
        }
    }

    public class BulkVideoMd5
    {
        public string Id { get; set; }
        public string Title { get; set; }
    }
}