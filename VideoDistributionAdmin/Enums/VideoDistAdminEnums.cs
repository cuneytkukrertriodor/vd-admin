﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VideoDistribution.Enums
{
    public class VideoDistAdminEnums
    {
        public enum VideoStatusEnum
        {
            Approved=1,
            Rejected=2,
            Watched=3,
            UnRejected=4
        }

        public enum EmailConfigEnum
        {
            NOREPLY = 1,
            INFO = 2,
            SUPPORT = 3,
            ADMIN = 4
        }
    }
}