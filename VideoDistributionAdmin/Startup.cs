﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(VideoDistribution.Startup))]
namespace VideoDistribution
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
