/**
 * Search Server
 * http://trac.backoffice.tibaco.net/development/wiki/SearchServer
 * @param phrase
 * @param callback
 */
function requestSearchResults(phrase, callback) {
    // Wait until the Tibaco search service is loaded.
    TWS.search.addReadyEventListener(() => {
        if (phrase && phrase.length >= 0 && typeof tws_Search === 'object') {

            // Remove special chars.
            var cleanPhrase = phrase.replace(/[^\w\s]/gi, '');

            // Create query string.
            var queryString = tws_Search.buildQueryString(cleanPhrase);
            var desktopQueryString = queryString.replace('AND', 'AND (isapp_text:"false"^1000 OR isapp_text:"true"^1) AND');

            // Do search request
            tws_Search.search(desktopQueryString, 60, 0, ['id', 'code', 'path_text', 'title_text', 'mediumDescription_text', 'rating_double', 'tile_text', 'screenshot_text']);

            // Create search callback
            tws_Search.setCallBackFunction(function() {
                var results = tws_Search.getResults();
                callback(results);
            });

        } else {
            console.info('No search results');
        }
    });
}

export { requestSearchResults }