import Page from '../pages/page';

import { embedFlash } from '../modules/embedFlash';

const GAME = window.GAME || {};

let instance = null;

class GamePage extends Page {

    static get instance() {
        return instance;
    }

    constructor() {
        super();

        // Make this a singleton.
        if (instance) {
            return instance;
        } else {
            instance = this;
        }

        return instance;
    }

    enableGame() {
        var stage = document.getElementById('game');
        // Simply check if the stage is there.
        if (!stage) {
            return;
        }
        // Simply check if touch is enabled. If so we simply assume our user is on a mobile device.
        // Currently too much to do right now to even bother about people with touch enabled netbooks and stuff. Sawry.
        if ('ontouchstart' in window || navigator.maxTouchPoints) {
            stage.setAttribute('data-embedded', 'false');
            return;
        }
        // Check if the game is resizable when its width is to big for the staging.
        if (!GAME.resizable && GAME.width > stage.offsetWidth) {
            stage.setAttribute('data-embedded', 'false');
            return;
        }
        if (GAME.type === 'html5' || GAME.type === 'webgl' || GAME.type === 'unity') {
            var gameFrame = game.getElementsByTagName('iframe')[0];
            if (typeof gameFrame === 'undefined') {
                gameFrame = document.createElement('iframe');
                game.appendChild(gameFrame);
            }
            // Here we set some iframe options. We also set focus() to the frame. Otherwise we might hit problems
            // with games not being able to change its own dimensions or registering touch events.
            gameFrame.scrolling = (GAME.disableScrollbars) ? 'no' : 'yes';
            gameFrame.setAttribute('allowFullScreen', '');
            gameFrame.src = GAME.location;
            gameFrame.style.width = (GAME.resizable) ? '100%' : GAME.width + 'px';
            gameFrame.style.height = (GAME.resizable) ? '100%' : GAME.height + 'px';
            gameFrame.focus();
            gameFrame.onload = () => {
                stage.setAttribute('data-embedded', 'true');
            };
            gameFrame.onerror = () => {
                stage.setAttribute('data-embedded', 'false');
            };
        } else if (GAME.type === 'flash' || GAME.type === 'swf') {
            var swf = embedFlash(GAME.location, {
                    id: 'swf',
                    width: (GAME.resizable) ? '100%' : GAME.width,
                    height: (GAME.resizable) ? '100%' : GAME.height
                }, {
                    allowScriptAccess: GAME.allowScriptAccess,
                    wmode: GAME.wmode,
                    menu: false,
                    bgcolor: '#000',
                    allowNetworking: GAME.allowNetworking
                }
            );
            stage.appendChild(swf);
            stage.setAttribute('data-embedded', 'true');
        } else {
            console.info(GAME.type + ' type of game is not able to run on gamedistribution.com.');
        }
    }

    enableVooxe() {
        var vooxeElement = document.getElementById('vooxe');
        if (typeof vooxeElement !== 'undefined') {
            var settings = {
                container: 'vooxe',
                gameId: GAME.id,
                publisherId: 'dc63a91fa184423482808bed4d782320',
                width: '100%',
                height: '100%',
                title: GAME.title,
                category: GAME.category,
                langCode: 'en',
                autoplay: 'no',
                onFound: function(data) {
                    // ...
                },
                onError: function(data) {
                    // ...
                },
                onReady: function(player) {
                    // ...
                }
            };
            (function(i, s, o, g, r, a, m) {
                i['VooxeVideo'] = r;
                i[r] = i[r] || function() {
                        (i[r].q = i[r].q || []).push(arguments)
                    };
                i[r].l = 1 * new Date();
                a = s.createElement(o);
                m = s.getElementsByTagName(o)[0];
                a.async = 1;
                a.src = g;
                m.parentNode.insertBefore(a, m);
            })(window, document, 'script', '//video-static.vooxe.com/libs/gd/gd.js', 'gdPlayer');
            gdPlayer(settings);
        }
    }

    enableClipboards() {
        var clips = document.getElementsByClassName('clipboard');
        if (typeof clips !== 'undefined') {
            for (var i = 0; i < clips.length; i++) {
                var textarea = clips[i].getElementsByTagName('textarea')[0];
                var button = clips[i].getElementsByTagName('button')[0];
                var type = clips[i].getAttribute('data-type');
                if (typeof textarea !== 'undefined' && typeof button !== 'undefined') {
                    // Populate the textarea.
                    var location = GAME.location,
                        scrolling = (GAME.disableScrollbars) ? 'no' : 'yes',
                        width = (GAME.resizable) ? '100%' : GAME.width + 'px',
                        height = (GAME.resizable) ? '100%' : GAME.height + 'px',
                        allowScriptAccess = GAME.allowScriptAccess,
                        wmode = GAME.wmode,
                        menu = 'false',
                        bgcolor = '#000',
                        allowNetworking = GAME.allowNetworking;
                    if (type === 'iframe') {
                        var embedHTML = '<iframe src="' + location + '" style="width: ' + width + '; height: ' + height + ';" scrolling="' + scrolling + '"></iframe>';
                        textarea.innerHTML = embedHTML;
                    } else if (type === 'flash') {
                        var embedHTML = '<object width="' + width + '" height="' + height + '" type="application/x-shockwave-flash" data="' + location + '"><param name="allowScriptAccess" value="' + allowScriptAccess + '"><param name="wmode" value="' + wmode + '"><param name="menu" value="' + menu + '"><param name="bgcolor" value="' + bgcolor + '"><param name="' + allowNetworking + '" value="all"></object>';
                        textarea.innerHTML = embedHTML;
                    } else {
                        textarea.innerHTML = GAME.location;
                    }
                    // Now bind the button.
                    button.addEventListener('click', function(e) {
                        var parent = e.target.parentNode;
                        var textarea = parent.getElementsByTagName('textarea')[0];
                        textarea.select();
                        try {
                            var successful = document.execCommand('copy');
                            var msg = successful ? 'successful' : 'unsuccessful';
                            console.log('Copying text command was ' + msg);
                        } catch (err) {
                            console.log('Oops, unable to copy');
                        }
                    });
                }
            }
        }
    }
}

export default GamePage;