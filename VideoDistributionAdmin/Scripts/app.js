﻿var app = angular.module("admin", ["ngRoute"]);

app.config(function ($httpProvider) {
    $httpProvider.interceptors.push(function ($q) {
        return {
            'request': function (config) {
                $('#processing').show();
                return config;
            },

            'response': function (response) {
                $('#processing').hide();
                return response;
            }
        };
    });
});

//app.config(function ($routeProvider) {
//    $routeProvider
//    .when("/allvideos", {
//        templateUrl: "/Videos/Index",
//        controller: "allVideosCtrl"
//    })
//    .when("/waitingvideos", {
//        templateUrl: "/VideoApprove/Index",
//        controller: "waitingVideosCtrl"
//    })
//    .when("/editvideo:videoid", {
//        templateUrl: "/Videos/Edit/" + '{{ ctrl.videoid }}',
//        controller: ['$routeParams', function ($routeParams) {
//            debugger;
//                var self = this;
//                self.videoid = $routeParams.videoid;
//            }],
//            controllerAs: 'ctrl'
//        })
//    .otherwise({
//        redirectTo: "/"
//    });
//});