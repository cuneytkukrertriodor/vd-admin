﻿app.controller("allVideosCtrl", function ($scope, $http, $routeParams) {

    //$scope.videoid = $routeParams.videoid;
    $scope.field = '';
    $scope.condition = '';
    $scope.operator = 0;
    $scope.value = '';
    $scope.pagenum = "1";
    $scope.sortdatafield = "Id";
    $scope.sortorder = "desc";
    $scope.TotalVideoCount = 0;

    $scope.FormatDate = function (date) {
        var day, month, year;
        
        var jsDate = new Date(date.match(/\d+/)[0] * 1);

        year = jsDate.getFullYear();

        if ((jsDate.getMonth() + 1) < 10)
            month = '0' + (jsDate.getMonth() + 1);
        else
            month = jsDate.getMonth() + 1;

        if (jsDate.getDate() < 10)
            day = '0' + jsDate.getDate();
        else
            day = jsDate.getDate();

        return day + '/' + month + '/' + year;
    };

    $scope.HandleLongText = function (text) {
        return text.substring(0, 15) + '...';
    };

    $scope.FilterAllVideos = function () {
        var filterQueryString = "filtervalue0=" + $scope.value + "&filtercondition0=" + $scope.condition + "&filteroperator0=" + $scope.operator + "&filterdatafield0=" + $scope.field
                                + "&sortdatafield=" + $scope.sortdatafield + "&sortorder=" + $scope.sortorder;

        if ($scope.value && $scope.value.length > 0)
            filterQueryString = "filterscount=1&" + filterQueryString;
        else
            filterQueryString = "filterscount=0&" + filterQueryString;
        
        $http({
            method: 'POST',
            url: '/Videos/GetVideos?' + filterQueryString,
            data: { sortdatafield: '', sortorder: '', pagesize: 100, pagenum: parseInt($scope.pagenum) - 1 },
        }).then(function successCallback(response) {
            $scope.AllVideos = response.data.Rows;
            $scope.Paging = Array(Math.ceil(response.data.TotalRows / 100)).fill().map((x, i) => (i + 1).toString());
            $scope.TotalVideoCount = response.data.TotalRows;
        }, function errorCallback(response) {
            
        });
    };

    $scope.UpdateFilter = function (field, condition, operator, value, pagenum) {
        $scope.field = field;
        $scope.condition = condition;
        $scope.operator = operator;
        $scope.value = value;
        $scope.pagenum = pagenum;
        $scope.FilterAllVideos();
    };

    $scope.Sort = function (sortdatafield, sortorder) {
        $scope.sortdatafield = sortdatafield;
        $scope.sortorder = sortorder;
        $scope.FilterAllVideos();
    };

    $scope.OpenVideoDetail = function (videoId) {
        var win = window.open('/Videos/Edit/' + videoId, '_blank');
        if (win) {
            win.focus();
        } else {
            alert('Please allow popups for this website');
        }
    };

    $scope.FilterAllVideos();
});

app.controller("waitingVideosCtrl", function ($scope, $http, $routeParams) {

    //$scope.videoId = $routeParams.videoId;
    $scope.reason = '';
    $scope.field = '';
    $scope.condition = '';
    $scope.operator = 0;
    $scope.value = '';
    $scope.pagenum = "1";
    $scope.sortdatafield = "AddedDate";
    $scope.sortorder = "desc";
    $scope.TotalVideoCount = 0;

    $scope.FormatDate = function (date) {
        var day, month, year;

        var jsDate = new Date(date.match(/\d+/)[0] * 1);

        year = jsDate.getFullYear();

        if ((jsDate.getMonth() + 1) < 10)
            month = '0' + (jsDate.getMonth() + 1);
        else
            month = jsDate.getMonth() + 1;

        if (jsDate.getDate() < 10)
            day = '0' + jsDate.getDate();
        else
            day = jsDate.getDate();

        return day + '/' + month + '/' + year;
    };

    $scope.HandleLongText = function (text) {
        return text.substring(0, 15) + '...';
    };

    $scope.FilterWaitingVideos = function () {
        var filterQueryString = "filtervalue0=" + $scope.value + "&filtercondition0=" + $scope.condition + "&filteroperator0=" + $scope.operator + "&filterdatafield0=" + $scope.field
                                + "&sortdatafield=" + $scope.sortdatafield + "&sortorder=" + $scope.sortorder;

        if ($scope.value && $scope.value.length > 0)
            filterQueryString = "filterscount=1&" + filterQueryString;
        else
            filterQueryString = "filterscount=0&" + filterQueryString;

        $http({
            method: 'POST',
            url: '/VideoApprove/GetVideos?' + filterQueryString,
            data: { sortdatafield: '', sortorder: '', pagesize: 100, pagenum: parseInt($scope.pagenum) - 1 },
        }).then(function successCallback(response) {
            $scope.WaitingVideos = response.data.Rows;
            $scope.WaitingPaging = Array(Math.ceil(response.data.TotalRows / 100)).fill().map((x, i) => (i + 1).toString());
            $scope.WaitingVideoCount = response.data.TotalRows;
        }, function errorCallback(response) {

        });
    };

    $scope.UpdateFilter = function (field, condition, operator, value, pagenum) {
        $scope.field = field;
        $scope.condition = condition;
        $scope.operator = operator;
        $scope.value = value;
        $scope.pagenum = pagenum;
        $scope.FilterWaitingVideos();
    };

    $scope.Sort = function (sortdatafield, sortorder) {
        $scope.sortdatafield = sortdatafield;
        $scope.sortorder = sortorder;
        $scope.FilterWaitingVideos();
    };

    $scope.OpenVideoDetail = function (videoId) {
        var win = window.open('/Videos/Edit/' + videoId +"?type=waiting", '_blank');
        if (win) {
            win.focus();
        } else {
            alert('Please allow popups for this website');
        }
    };

    $scope.BulkApprove = function () {
        var selectedVideos = [];

        $.each(document.querySelectorAll('[id^=cb_]'), function (index, value) {
            if (value.checked === true)
                selectedVideos.push(value.id.replace('cb_', ''));
        });

        if (selectedVideos.length > 0) {
            $http({
                method: 'POST',
                url: '/VideoApprove/BulkApprove',
                data: { videoMd5List: JSON.stringify(selectedVideos) },
            }).then(function successCallback(response) {
                alert('Bulk approve is successfull');
            }, function errorCallback(response) {
                alert('Error in bulk approve!!!');
            });
        }
        else {
            alert('You should select at least one video!');
        }
    };

    $scope.BulkDelete = function () {
        var selectedVideos = [];
        
        $.each(document.querySelectorAll('[id^=cb_]'), function (index, value) {
            if (value.checked === true)
                selectedVideos.push({ 'Id': value.id.replace('cb_', ''), 'Title': value.name});
        });

        if (selectedVideos.length > 0) {
            if ($scope.reason && $scope.reason.length > 0) {
                $http({
                    method: 'POST',
                    url: '/VideoApprove/BulkDeleteVideo',
                    data: { videoMd5List: JSON.stringify(selectedVideos), reason: $scope.reason },
                }).then(function successCallback(response) {
                    alert('Bulk delete is successfull');
                }, function errorCallback(response) {
                    alert('Error in bulk delete!!!');
                });
            }
            else {
                alert('You should fill the reason field!');
            }
        }
        else {
            alert('You should select at least one video!');
        }
    };

    $scope.BulkReject = function () {
        var selectedVideos = [];

        $.each(document.querySelectorAll('[id^=cb_]'), function (index, value) {
            if (value.checked === true)
                selectedVideos.push({ 'Id': value.id.replace('cb_', ''), 'Title': value.name });
        });

        if (selectedVideos.length > 0) {
            if ($scope.reason && $scope.reason.length > 0) {
                $http({
                    method: 'POST',
                    url: '/VideoApprove/BulkRejected',
                    data: { videoMd5List: JSON.stringify(selectedVideos), reason: $scope.reason },
                }).then(function successCallback(response) {
                    alert('Bulk delete is successfull');
                }, function errorCallback(response) {
                    alert('Error in bulk delete!!!');
                });
            }
            else {
                alert('You should fill the reason field!');
            }
        }
        else {
            alert('You should select at least one video!');
        }
    };

    $scope.SelectAll = function (checked) {
        $.each(document.querySelectorAll('[id^=cb_]'), function (index, value) {
            value.checked = checked;
        });
    };

    $scope.FilterWaitingVideos();
});

app.controller("reportCtrl", function ($scope, $http, $routeParams) {

    $scope.currentMonth = '';
    $scope.currentWeek = '';

    $scope.FormatDate = function (date, isJsDateAlready) {
        var day, month, year;
        var jsDate = date;
        if (!isJsDateAlready)
        {
            jsDate = new Date(jsDate.match(/\d+/)[0] * 1);
        }

        year = jsDate.getFullYear();

        if ((jsDate.getMonth() + 1) < 10)
            month = '0' + (jsDate.getMonth() + 1);
        else
            month = jsDate.getMonth() + 1;

        if (jsDate.getDate() < 10)
            day = '0' + jsDate.getDate();
        else
            day = jsDate.getDate();

        return day + '/' + month + '/' + year;
    };

    $scope.GetCurrentMonth = function () {
        var monthNames = ["January", "February", "March", "April", "May", "June",
                              "July", "August", "September", "October", "November", "December"];
        var d = new Date();
        $scope.currentMonth = monthNames[d.getMonth()];
    };

    $scope.GetCurrentMondayAndSunday = function () {
        d = new Date();
        var day = d.getDay(),
            diff = d.getDate() - day + (day == 0 ? -6 : 1); // adjust when day is sunday
        var mDate = new Date(d.setDate(diff));
        var sDate = new Date(d.setDate(diff + 6));
        $scope.currentWeek = $scope.FormatDate(mDate, true) + ' - ' + $scope.FormatDate(sDate, true);
    };

    $scope.HandleLongText = function (text) {
        return text.substring(0, 15) + '...';
    };

    $scope.GetCurrentMonth();
    $scope.GetCurrentMondayAndSunday();
});

app.controller("videoReportCtrl", function ($scope, $http, $routeParams) {

    $scope.currentMonth = '';
    $scope.currentWeek = '';

    $scope.FormatDate = function (date, isJsDateAlready) {
        var day, month, year;
        var jsDate = date;
        if (!isJsDateAlready) {
            jsDate = new Date(jsDate.match(/\d+/)[0] * 1);
        }

        year = jsDate.getFullYear();

        if ((jsDate.getMonth() + 1) < 10)
            month = '0' + (jsDate.getMonth() + 1);
        else
            month = jsDate.getMonth() + 1;

        if (jsDate.getDate() < 10)
            day = '0' + jsDate.getDate();
        else
            day = jsDate.getDate();

        return day + '/' + month + '/' + year;
    };

    $scope.GetCurrentMonth = function () {
        var monthNames = ["January", "February", "March", "April", "May", "June",
                              "July", "August", "September", "October", "November", "December"];
        var d = new Date();
        $scope.currentMonth = monthNames[d.getMonth()];
    };

    $scope.GetCurrentMondayAndSunday = function () {
        d = new Date();
        var day = d.getDay(),
            diff = d.getDate() - day + (day == 0 ? -6 : 1); // adjust when day is sunday
        var mDate = new Date(d.setDate(diff));
        var sDate = new Date(d.setDate(diff + 6));
        $scope.currentWeek = $scope.FormatDate(mDate, true) + ' - ' + $scope.FormatDate(sDate, true);
    };

    $scope.HandleLongText = function (text) {
        return text.substring(0, 15) + '...';
    };

    $scope.GetCurrentMonth();
    $scope.GetCurrentMondayAndSunday();
});

app.controller("publisherDomainsCtrl", function ($scope, $http, $routeParams) {

    //$scope.videoId = $routeParams.videoId;
    $scope.field = '';
    $scope.condition = '';
    $scope.operator = 0;
    $scope.value = '';
    $scope.pagenum = "1";
    $scope.sortdatafield = "AddedDate";
    $scope.sortorder = "desc";
    $scope.TotalVideoCount = 0;

    $scope.FormatDate = function (date) {
        var day, month, year;

        var jsDate = new Date(date.match(/\d+/)[0] * 1);

        year = jsDate.getFullYear();

        if ((jsDate.getMonth() + 1) < 10)
            month = '0' + (jsDate.getMonth() + 1);
        else
            month = jsDate.getMonth() + 1;

        if (jsDate.getDate() < 10)
            day = '0' + jsDate.getDate();
        else
            day = jsDate.getDate();

        return day + '/' + month + '/' + year;
    };

    $scope.HandleLongText = function (text) {
        return text.substring(0, 15) + '...';
    };

    $scope.FilterPublisherDomains = function () {
        var filterQueryString = "filtervalue0=" + $scope.value + "&filtercondition0=" + $scope.condition + "&filteroperator0=" + $scope.operator + "&filterdatafield0=" + $scope.field
                                + "&sortdatafield=" + $scope.sortdatafield + "&sortorder=" + $scope.sortorder;

        if ($scope.value && $scope.value.length > 0)
            filterQueryString = "filterscount=1&" + filterQueryString;
        else
            filterQueryString = "filterscount=0&" + filterQueryString;

        $http({
            method: 'POST',
            url: '/Account/GetPublisherDomains?' + filterQueryString,
            data: { sortdatafield: '', sortorder: '', pagesize: 100, pagenum: parseInt($scope.pagenum) - 1 },
        }).then(function successCallback(response) {
            $scope.PublisherDomains = response.data.Rows;
            $scope.PublisherPaging = Array(Math.ceil(response.data.TotalRows / 100)).fill().map((x, i) => (i + 1).toString());
            $scope.PublisherDomainsCount = response.data.TotalRows;
        }, function errorCallback(response) {

        });
    };

    $scope.UpdateFilter = function (field, condition, operator, value, pagenum) {
        $scope.field = field;
        $scope.condition = condition;
        $scope.operator = operator;
        $scope.value = value;
        $scope.pagenum = pagenum;
        $scope.FilterPublisherDomains();
    };

    $scope.Sort = function (sortdatafield, sortorder) {
        $scope.sortdatafield = sortdatafield;
        $scope.sortorder = sortorder;
        $scope.FilterPublisherDomains();
    };

    $scope.ChangeAdsEnabled = function (id, state) {
        debugger;
        $http({
            method: 'POST',
            url: '/Account/ChangeAdsEnabled',
            data: { id: id, state: state },
        }).then(function successCallback(response) {
            if (state === true)
                alert('Enabling ads is successfull');
            else
                alert('Disabling ads is successfull');

            $scope.FilterPublisherDomains();
        }, function errorCallback(response) {
            alert('Changing ads has an error!!!');
        });
    };

    $scope.FilterPublisherDomains();
});