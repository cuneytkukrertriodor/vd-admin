﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using static VideoDistribution.Enums.VideoDistAdminEnums;

namespace VideoDistribution.Models
{
    public class EmailConfigModel
    {
        public EmailConfigEnum ConfigType { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string DisplayName { get; set; }
        public string Host { get; set; }
        public int Port { get; set; }
        public bool EnableSsl { get; set; }
    }
}