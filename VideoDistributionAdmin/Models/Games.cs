﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VideoDistribution.Models
{
    public class Games
    {
        public string CatTitle { get; set; }
        public string Description { get; set; }
        public int Width { get; set; }
        public int Height { get; set; }
        public DateTime AddedDate { get; set; }
        public DateTime ActivedDate { get; set; }
    }
}