﻿namespace VideoDistribution.Models
{
    public class TagModel
    {
        public int TagId { get; set; }
        public string Title { get; set; }
    }
}