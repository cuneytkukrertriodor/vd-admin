﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VideoDistribution.Models
{
    public class ApproveVideoViewModel
    {
        public int UserId { get; set; }
        public int VideoId { get; set; }
        public string VideoMd5 { get; set; }
        public string Name { get; set; }
        public string Lastname{ get; set; }
        public bool Approved { get; set; }
        public DateTime AddedDate { get; set; }
        public string Title { get; set; }
        public string ExternalURL { get; set; }
        public string Description { get; set; }
        public long? Duration { get; set; }
        public string ExternalThumbURL { get; set; }
        public List<TagModel> Tags { get; set; }
        public string URL { get; set; }
    }
}